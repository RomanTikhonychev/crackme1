//
//  ViewController.m
//  CrackMe
//
//  Created by Roman Tikhonychev on 7/19/17.
//  Copyright © 2017 com.roman. All rights reserved.
//

#import "ViewController.h"
#import <objc/runtime.h>

int hookTargetFunction()
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"Activated"];
}

@interface NSMutableString (UAObfuscatedString)

- (instancetype)a;
- (instancetype)b;
- (instancetype)c;
- (instancetype)d;
- (instancetype)e;
- (instancetype)f;
- (instancetype)g;
- (instancetype)h;
- (instancetype)i;
- (instancetype)j;
- (instancetype)k;
- (instancetype)l;
- (instancetype)m;
- (instancetype)n;
- (instancetype)o;
- (instancetype)p;
- (instancetype)q;
- (instancetype)r;
- (instancetype)s;
- (instancetype)t;
- (instancetype)u;
- (instancetype)v;
- (instancetype)w;
- (instancetype)x;
- (instancetype)y;
- (instancetype)z;

@end

#define inlineIsActivatedCheck      superViewDidLoad
#define isActivated                 viewHasSubview
#define replaceIsActivatedMethods   hideLayer

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self replaceIsActivatedMethods];
}

- (IBAction)doSomeWork:(id)sender
{
    if (hookTargetFunction() == YES)
    {
        NSLog(@"Do some work");
        NSString *counterString = [NSString stringWithFormat:@"%li", ((long)self.counterLabel.integerValue + 1)];
        [self.counterLabel setStringValue:counterString];
    }
    else
    {
        [self showCrackedAlert];
    }
}

#pragma mark - Hide strings 



- (void)showCrackedAlert
{
    NSMutableString *alertMessageText = [[NSMutableString alloc]initWithString:@"a"];
    alertMessageText = alertMessageText.p.p.l.i.c.a.t.i.o.n.c.r.a.c.k.e.d;
    
    NSAlert *alert = [NSAlert alertWithMessageText:alertMessageText
                                     defaultButton:@"OK"
                                   alternateButton:@"Cancel"
                                       otherButton:nil
                         informativeTextWithFormat:@"Buy with discount"];
    
    [alert beginSheetModalForWindow:self.view.window completionHandler:^(NSModalResponse returnCode)
    {
        NSArray *crashArray = @[@"I'am", @"gonna", @"crash"];
        NSString *crashString = crashArray[4];
        NSLog(@"%@", crashString);
    }];
}

#pragma mark - isActivated

static __attribute__((always_inline)) BOOL inlineIsActivatedCheck(void)
{
    return [[ViewController new] isActivated];
}


- (BOOL)isActivated
{
    NSLog(@"Function called");
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"Activated"];
}

- (void)replaceIsActivatedMethods
{
    Class validatorClass = [ViewController class];
    
    BOOL (^isActivatedBlock)(void) = ^BOOL(void)
    {
        NSLog(@"Block called");
        return [[NSUserDefaults standardUserDefaults] boolForKey:@"Activated"];
    };
    
    class_replaceMethod(validatorClass, @selector(isActivated), imp_implementationWithBlock(isActivatedBlock), NULL);
}


@end


@implementation NSMutableString (UAObfuscatedString)

#pragma mark - a-z -
- (instancetype)a { [self appendString:@"a"]; return self; }
- (instancetype)b { [self appendString:@"b"]; return self; }
- (instancetype)c { [self appendString:@"c"]; return self; }
- (instancetype)d { [self appendString:@"d"]; return self; }
- (instancetype)e { [self appendString:@"e"]; return self; }
- (instancetype)f { [self appendString:@"f"]; return self; }
- (instancetype)g { [self appendString:@"g"]; return self; }
- (instancetype)h { [self appendString:@"h"]; return self; }
- (instancetype)i { [self appendString:@"i"]; return self; }
- (instancetype)j { [self appendString:@"j"]; return self; }
- (instancetype)k { [self appendString:@"k"]; return self; }
- (instancetype)l { [self appendString:@"l"]; return self; }
- (instancetype)m { [self appendString:@"m"]; return self; }
- (instancetype)n { [self appendString:@"n"]; return self; }
- (instancetype)o { [self appendString:@"o"]; return self; }
- (instancetype)p { [self appendString:@"p"]; return self; }
- (instancetype)q { [self appendString:@"q"]; return self; }
- (instancetype)r { [self appendString:@"r"]; return self; }
- (instancetype)s { [self appendString:@"s"]; return self; }
- (instancetype)t { [self appendString:@"t"]; return self; }
- (instancetype)u { [self appendString:@"u"]; return self; }
- (instancetype)v { [self appendString:@"v"]; return self; }
- (instancetype)w { [self appendString:@"w"]; return self; }
- (instancetype)x { [self appendString:@"x"]; return self; }
- (instancetype)y { [self appendString:@"y"]; return self; }
- (instancetype)z { [self appendString:@"z"]; return self; }

@end
