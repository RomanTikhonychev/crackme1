//
//  AppDelegate.h
//  CrackMe
//
//  Created by Roman Tikhonychev on 7/19/17.
//  Copyright © 2017 com.roman. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

