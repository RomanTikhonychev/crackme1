//
//  AppDelegate.m
//  CrackMe
//
//  Created by Roman Tikhonychev on 7/19/17.
//  Copyright © 2017 com.roman. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{    
    NSLog(@"applicationDidFinishLaunching");
}

@end
