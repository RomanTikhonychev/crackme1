//
//  main.m
//  CrackMe
//
//  Created by Roman Tikhonychev on 7/19/17.
//  Copyright © 2017 com.roman. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
